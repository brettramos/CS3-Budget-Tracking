import { useState, useEffect } from 'react';
import { Container } from 'react-bootstrap';
import '../styles/globals.css'
import 'bootstrap/dist/css/bootstrap.min.css'
import style from '../styles/style.css'
import Navbar from '../components/NavBar';
import { UserProvider } from '../UserContext';

// import '../styles/list.css'

export default function MyApp({ Component, pageProps }) {
	//state hook for the user state here for global scoping
	const [user, setUser] = useState({
		//value is initialized as an object for the user with the properties set to null
		//proper values will be obtained from the local storage AFTER component gets rendered due to NextJS pre rendering.
		// email: null,
		// isAdmin: null
		id: null, 
		isAdmin: null,
		firstName: null,
		lastName: null,
		email: null,
		userBalance: null,
		transactions: null,
		incomeCategories: null,
		expenseCategories: null		
	})

	//localStorage can only be accessed after this component has been rendered, hence the "need" for an effect hook
	useEffect(() => {
		setUser({
			// email: localStorage.getItem('email'),
			// isAdmin: localStorage.getItem('isAdmin') === 'true'
			id: localStorage.getItem('userId'), 
			isAdmin: localStorage.getItem('isAdmin'),
			firstName: localStorage.getItem('firstName'),
			lastName: localStorage.getItem('lastName'),
			email: localStorage.getItem('email'),
			transactions: localStorage.getItem('transactions'),
			incomeCategories: localStorage.getItem('incomeCategories'),
			expenseCategories: localStorage.getItem('expenseCategories')		
		})
	}, [])

	//lets create an effect hook for testing the setUser() functionality
	// useEffect(() => {
	// 	// console.log(`${user} naka login`);
	// 	// console.log({user});
	// }, [user])
  
	//lets create a function for our logout component that will clear out the date from our local storage.
	const unsetUser = () => {
		localStorage.clear()
		//lets reset the user global scope in the context provider to have its email and isAdmin props reverted to its initial value/state
		setUser({
			email: null,
			isAdmin: null
		});
	}
  return(
  	<>
	{/*wrapt the component tree within the UserProvider context provider so that the component will have access to the passed values in this module.*/}
    <UserProvider value={{user, setUser, unsetUser}}>
    	<Navbar />
	  	<Container >
	  	<Component {...pageProps} />
	  	</Container>
	</UserProvider> 
	</> 	
  	) 
}
