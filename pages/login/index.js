import { useState, useContext, useEffect } from 'react';
import { Form, Button, Row, Col, Card, Image } from 'react-bootstrap';
import UserContext from '../../UserContext';
import users from '../../data/users';
import Router from 'next/router';
import Head from 'next/head';
import View from '../../components/View';
import { GoogleLogin } from 'react-google-login';
import AppHelper from '../../app-helper';
import Swal from 'sweetalert2';

// export default () => {
const Login = () => {
	return(
			<View title={'Login'}>
				<Row className="justify-content-center">
					<Col md="4">
		          	<Image src="smaller-ant-icon-10.jpg" fluid/>
		         	</Col>
				</Row>					
				<Row className="justify-content-center">
					<Col xs md="4"> 
					  {/*<h5 className="text-center">BUDGET TRACKER</h5>*/} 
					   <LoginForm />
					</Col>
				</Row>
			</View>				
		)
}
export default Login;
// }		
	const LoginForm = () => {
		const { user, setUser } = useContext(UserContext)
		
		const [ email, setEmail] = useState('');
		const [ password, setPassword] = useState('');
		const [ tokenId, setTokenId] = useState(null);	

		//lets create a function that will simulate a login page/user authentication
		function authenticate(e){
			// alert("hello from inside authenticate login.js")
			e.preventDefault();//prevent redirection via form submission
			//authentication based on imported users data
			//instead of a simmulation for a user authentication we are now going to modify this method to be an actual fetch request to save the gmail user inside the database
			const options = {
				method: 'POST',
				headers: { 'Content-Type': 'application/json'},
				body: JSON.stringify({ email: email, password: password })
			}
			//lets wrap/inject the behavior inside a control structure so that we will be able to identify the responses in their specific conditions
			// alert("hello from before FETCH login")
			fetch(`${ AppHelper.API_URL }/users/login`, options).then(AppHelper.toJSON).then(data => {
				// alert("OKhello from inside FETCH login.js")
				console.log(data)
				if(typeof data.access !== 'undefined'){
				localStorage.setItem('token', data.access)
				// alert("NOPEhello from before retrieveUserDetails login.js")
				retrieveUserDetails(data.access)
				} else {
					//inside this branch we are going to give it these exact conditions upon failure 
					if(data.error === 'does-not-exist'){
						Swal.fire('Authentication Failed', 'User does not exist', 'error')
					} else if (data.error === 'incorrect-password'){
						Swal.fire('Authentication Failed', 'Password is incorrect', 'error')
					} else if (data.error === 'login-type-error') {
						Swal.fire('Login Type Error', 'You may have registered through a different login procedure, try using an alternative login procedure', 'error' )
					} else {
						console.log(data.error)
					}			
				}		
			})
		}
	
		const retrieveUserDetails = (accessToken) => {
			
			const options = {
				headers: { Authorization: `Bearer ${accessToken}` }			
			}
			fetch(`${AppHelper.API_URL}/users/details`, options).then(AppHelper.toJSON).then(data => {
				setUser({ 	id: data._id, 
							isAdmin: data.isAdmin,
							firstName: data.firstName,
							lastName: data.lastName,
							email: data.email,
							transactions: data.transactions,
							incomeCategories: data.incomeCategories,
							expenseCategories: data.expenseCategories						})
				localStorage.setItem('user', data)
				// console.log("id: " + data._id + "  isAdmin: " + data.isAdmin )
				localStorage.setItem('userId', data._id)

				localStorage.setItem('isAdmin', data.isAdmin),
				localStorage.setItem('firstName', data.firstName),
				localStorage.setItem('lastName', data.lastName),
				localStorage.setItem('email', data.email),
				localStorage.setItem('transactions', data.transactions),
				localStorage.setItem('incomeCategories', data.incomeCategories),
				localStorage.setItem('expenseCategories', data.expenseCategories)

				Router.push('/dashboard')
			})
		}

		const authenticateGoogleToken = (response) => {
			console.log('YYYYYYYYYYYYYYYYYYYYYYYYYYYY'+response)
			console.log(response)
			localStorage.setItem('googleToken', response.accessToken)
			const payload = {
				method: 'post',
				headers: { 'Content-Type': 'application/json'},
				body: JSON.stringify({ tokenId: response.tokenId })
			}
			fetch(`${AppHelper.API_URL}/users/verify-google-id-token`, payload)
				.then(AppHelper.toJSON)
				.then(data => {
					if(typeof data.accessToken !== 'undefined') {
						localStorage.setItem('token', data.accessToken)
						retrieveUserDetails(data.accessToken) 
					} else {
						if(data.error === 'google-auth-error'){
							Swal.fire(
								'Google Auth Error',
								'Google Authentication failed',
								'error'
							)
						} else if (data.error === 'login-type-error'){
							Swal.fire(
								'Login Type Error',
								'You may have registered through a different login procedure',
								 'error'
							)
						}
					}
				})
		}	

		return(
				<>
					<h5 className="text-center">A NECESSARY TRACKER</h5>
					<Form onSubmit={ authenticate }>
						<Form.Group controlId="userEmail" className="mb-2">
							<Form.Label>Email Address</Form.Label>
							<Form.Control type="email" value={email} onChange={(e) => setEmail(e.target.value)} autoComplete="off" required/>
						</Form.Group>
						<Form.Group controlId="password" className="mb-2">
							<Form.Label>Password</Form.Label>
							<Form.Control type="password" value={password} onChange={(e) => setPassword(e.target.value)} required/>
						</Form.Group>	
						<Button className="bg-warning rounded-lg border-white text-dark mb-1" type="submit" block> Login </Button>

						<GoogleLogin 
							clientId="55028407660-53i2e81rrbpl180hunv7pk5e70n0cp2l.apps.googleusercontent.com"
							buttonText="Login using Google"
							onSuccess={ authenticateGoogleToken }
							onFailure={ authenticateGoogleToken }
							cookiePolicy={ 'single_host_origin' }
							className="w-100 text-center d-flex justify-content-center"
						/>			

					</Form>
				</>
			)
	}	
	