import { useState, useEffect } from 'react';
import Head from 'next/head';
import { Form, Button } from 'react-bootstrap';
import Swal from 'sweetalert2';
import AppHelper from '../../app-helper';
import Router from 'next/router';

export default function index(){
	
	const [ formData, setFormData ] = useState({
		firstName: "",
		lastName: "",
		email: "",
		password: "",
		confirmPassword: "",
		mobileNo: ""
	})

	const { firstName, lastName, email, password, confirmPassword, mobileNo } = formData
	
	const onChangeHandler = e => {
		setFormData({
			...formData,
			[e.target.name]: e.target.value
		})
	}
	
	const [isActive, setIsActive] = useState(false);
	
	useEffect(() => {		
		
	}, )

	
	function registerUser(e){
		e.preventDefault();
		
		if((firstName !== '' && lastName !== '' && email !== '' && password !== '' && confirmPassword !== '' && mobileNo !== '' )&&(mobileNo.length === 11)&&(confirmPassword === password)){
			fetch('http://localhost:4000/api/users/email-exists', {
				method: 'POST',
				headers: {
					'Content-Type' : 'application/json'
				},
				body: JSON.stringify({
					email: email
				})
			})
			.then(res => res.json())
			.then(data => {
				
				if(data === false){
					fetch('http://localhost:4000/api/users', {
						method: 'POST',
						headers: {
							'Content-Type': 'application/json'
						},
						body: JSON.stringify(formData)
					})
					.then(res => {
						return res.json()
					})
					.then(data => {
						console.log(data)
						
						if(data === true){
							Swal.fire({
								icon: 'success',
								title: 'New Account Registered Successfully',
								text: 'You May Now Log In'
							})
							Router.push('/login')
						} else {
							Swal.fire({
								icon: 'warning',
								title: 'Something Went Wrong, Check Credentials!'
							})
						}
					})
				} else {
					Swal.fire({
								icon: 'warning',
								title: 'Email has already been registered!'
							})
				}
			})
		}else {
			Swal.fire('Something Went Wrong!')
		}

		e.target.reset()	
	}
	return(
		<>
			<Head>		
				<title> Register Here </title>
			</Head>
			<h3>Register Here</h3>
			<Form onSubmit={(e) => registerUser(e)}>
				<div className="form-group">
					<input required type="text" id="firstName" name="firstName" className="form-control" value={firstName} placeholder="Insert First Name Here" autoComplete="off" onChange={e => onChangeHandler(e)}/>
				</div>
				<div className="form-group">
					<input required type="text" id="lastName" name="lastName" className="form-control" value={lastName} placeholder="Insert Last Name Here" autoComplete="off" onChange={e => onChangeHandler(e)}/>
				</div>
				<div className="form-group">
					<input required type="email" id="email" name="email" className="form-control" value={email} placeholder="Insert Email Here" autoComplete="off" onChange={e => onChangeHandler(e)}/>
				</div>
				<div className="form-group">
					<input  required type="password" id="password" name="password" className="form-control" value={password} placeholder="Insert Password Here" autoComplete="off" onChange={e => onChangeHandler(e)}/>
				</div>
				<div className="form-group">
					<input required type="password" id="confirmPassword" name="confirmPassword" className="form-control" value={confirmPassword} placeholder="Confirm Password Here" autoComplete="off" onChange={e => onChangeHandler(e)}/>
				</div>
				<div className="form-group">
					<input required type="number" id="mobileNo" name="mobileNo" className="form-control" value={mobileNo} placeholder="Insert Mobile Number Here" autoComplete="off" onChange={e => onChangeHandler(e)}/>
				</div>
				<button type="submit" className="btn btn-success">Register</button>							
			</Form>
		</>		
		)
}