import { useState, useContext, useEffect } from 'react';
import { Form, Button, Row, Col, Card } from 'react-bootstrap';
import UserContext from '../../UserContext';
import users from '../../data/users';
import Router from 'next/router';
import Head from 'next/head';
import View from '../../components/View';
// import { GoogleLogin } from 'react-google-login';
import AppHelper from '../../app-helper';
import Swal from 'sweetalert2';

import Select from '../../components/Select';

export default function AddNewTransaction ({categoryData}){

	return(
			<View title={'Dashboard'}>
					<Col xs md="12"> 
					
					   <AddTransaction />
					</Col>
			</View>				
		)
}

const AddTransaction = (e) => {		

		const { user, setUser } = useContext(UserContext)
		setUser(user)		

		const [ formData, setFormData ] = useState({
		transDate: "",
		transAmt: "",
		transType: "",
		transCategory: "",
		transNote: "",
		})

		const { transDate, transAmt, transType, transCategory, transNote } = formData

		//create a function that will allow us to target the state getter of the input fields and change their state
		const onChangeHandler = e => {
			setFormData({
				...formData,
				[e.target.name]: e.target.value,
				userId: localStorage.getItem('userId')
			})
		}

		function addUserTransaction(e){
			e.preventDefault()

			if(transAmt !== '' && transType !== '' && transCategory !== '' && transNote !== ''){
					// fetch(`${ AppHelper.API_URL }/users/addUserTransaction`, {
					fetch(`${ AppHelper.API_URL }/transactions/addTransaction`, {	
						method: 'POST',
						headers: {
							'Content-Type': 'application/json'
						},
						body: JSON.stringify(formData)
					})
					.then(res => {
						return res.json()
					})
					.then(data => {
						console.log(data)
			
						if(data === true){
							Swal.fire({
								icon: 'success',
								title: 'New Transaction Added'
							})

							fetch(`${ AppHelper.API_URL }/users/updateBalance`, {
								method: 'POST',
								headers: {
									'Content-Type': 'application/json'
								},
								body: JSON.stringify(formData)
							})
							.then(res => {
								return res.json()
							})

						} else {
							Swal.fire({
								icon: 'warning',
								title: 'Something Went Wrong, Check Credentials!'
							})
						}
					})
			
			}else {
				Swal.fire('Something Went Wrong!')
			}		

			e.target.reset()
			Router.push('/dashboard')
		}//END function addUserTransaction()		

		return(
			<>															
			<Card>				
				<Card.Body>					
					<h5 className="text-center">Add New Transaction</h5>
					<>
						<Form onSubmit={(e) => addUserTransaction(e)}>				
							<div className="form-group">
								<input required type="date" id="transDate" name="transDate" className="form-control"  placeholder="Transaction Date" autoComplete="off" onChange={e => onChangeHandler(e)}/>
							</div>
							
							<div className="form-group">
								<input required type="number" id="transAmt" name="transAmt" className="form-control"  placeholder="Amount in PhP" autoComplete="off" onChange={e => onChangeHandler(e)}/>
							</div>
						
							<div className="form-group">
                                <select required className="form-control" name="transType" defaultValue="" onChange={e => onChangeHandler(e)}>
                                	<option value="">Expense or Income?</option>
                                    <option value="expense">Expense</option>
                                    <option value="income">Income</option>
                                </select>
                            </div>

							<div className="form-group">
							<Select name="transCategory" options={ (transType === "expense" ) ? user.expenseCategories : user.incomeCategories } value={ (user.categories === transCategory) ? transCategory : null } onChange={e => onChangeHandler(e)} />

							<a href="/dashboard/categorySettings" className="btn btn-info">+ADD Category</a>	


							</div>	

                            
							<div className="form-group">
								<input required type="text" id="transNote" name="transNote" className="form-control" placeholder="Description" autoComplete="off" onChange={e => onChangeHandler(e)}/>
							</div>

							<Button type="submit" className="btn btn-success" block>Post Transaction</Button>							
						</Form>
					</>		
				</Card.Body>				
			</Card>										
			</>
		)//END return() addNewTransaction
	}//END export default ADDNEWTRANSACTION