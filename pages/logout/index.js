import { useContext, useEffect } from 'react';
import Router from 'next/router';
import UserContext from '../../UserContext';//lets acquire first the needed components/ data in order to build this new page.

//once the user targeted the logout component the goal is to redirect the user to the login page.
export default function index(){
	//consume the UserContext object and destructure it to access the user properties,
	const { unsetUser } = useContext(UserContext)

	//lets invoke the unsetUser only after the initial render
	useEffect(() => {
		//we will now acquire the unsetUser to clear the items/data inside our local storage.
		try { unsetUser() } catch (e) {console.log(e); }
		Router.push('/login')
	})
	return null;
}