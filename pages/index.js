import Head from 'next/head'
import styles from '../styles/Home.module.css'
import Banner from '../components/Banner';
import Highlights from '../components/Highlights';//lets now acquire the components we want to see
import { Container, Button, Row, Col, Image } from 'react-bootstrap';

//lets create a function called Home
export default function Home() {
  const data = {
    title: "A Necessary Tracker",
    content: "Budget Tracking App",
    destination: "/register",
    label: "Start Tracking Your Money!"
  }
    return(
    <>
      <Container>
        <Row>
           <Col md={4}>
            <Image src="ant-jumbotron2.jpg" fluid/>
           </Col>
           <Col>
          <Banner dataProp={data}/>
          </Col>
        </Row>
        <Row>
          <Col>
             <Highlights />
          </Col>       
        </Row>
      </Container>
    </>   
    )
}