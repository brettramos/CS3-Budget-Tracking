import { useContext, useState } from 'react';
import TransactionRow from '../../components/TransactionRow';
import { Table, Button, Container, Form } from 'react-bootstrap';
import UserContext from '../../UserContext';
import Head from 'next/head';

import Select from '../../components/Select';

export async function getServerSideProps(){
   const res = await fetch('http://localhost:4000/api/transactions')
                    
   const transactionsData = await res.json()

   return { props: { transactionsData }}
}

export default function index({transactionsData}){


  const [ formData, setFormData ] = useState({   
    searchItem: ""
    })

  const { searchItem } = formData

    const onChangeHandler = e => {
      setFormData({
        ...formData,
        [e.target.name]: e.target.value
      })
    }

	const transactions = transactionsData.map(transactionData => {
    return (
        <TransactionRow key={transactionData._id} transactionProp={transactionData}/>
      )
  }) 		
	
	const transactionRows = transactionsData.map(transactionData => {

    return(
        <tr key={transactionData._id}>
            <td>{transactionData._id}</td>
            <td>{transactionData.transType}</td>
            <td>{transactionData.transCategory}</td>
            <td>{transactionData.transAmt}</td>
            <td>{transactionData.transDate}</td>
            <td>{transactionData.transNote}</td>
        </tr>
      )
  })
   
		return (
          //  
            <Container>
             <Head>
                 <title>Transactions</title>
             </Head>

            <>
            <Form >        
              
              <div className="form-group">
                <input required type="text" id="searchItem" name="searchItem" className="form-control mt-5" placeholder="What are you looking for?" autoComplete="off" onChange={e => onChangeHandler(e)}/>
              </div>

              <Button type="submit" className="btn btn-success mb-5" block>Search</Button>             
            </Form>
            </>

              <Table striped bordered hover>
                    <thead>
                       <tr>
                            <th>Type</th>
                            <th>Category</th>
                            <th>Amount</th>
                            <th>Date</th>
                            <th>Purpose</th>                   
                       </tr>
                    </thead>          
                <tbody>
             { transactions }             
                </tbody>
              </Table>
            </Container> 		
		)
}