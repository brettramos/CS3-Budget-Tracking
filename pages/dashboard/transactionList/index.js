import { useState, useContext, useEffect } from 'react';
import { Form, Button, Row, Col, Card, Table, Container } from 'react-bootstrap';

import Router from 'next/router';
import Head from 'next/head';
import View from '../../../components/View';
import Swal from 'sweetalert2';

import UserContext from '../../../UserContext';
// import { useContext } from 'react';
import TransactionCard from '../../../components/TransactionCard';
import AppHelper from '../../../app-helper';

import Select from '../../../components/Select';

import TransactionsContext from '../../../TransactionsContext';

export default function TransactionList(){

	const { user, setUser } = useContext(UserContext)
	const accessToken = localStorage.getItem('token')

	const { transactions, setTransactions } = useContext;

	const retrieveUserDetails = (accessToken) => {
			
			const options = {
				// headers: { Authorization: `Bearer ${accessToken}` }			
			}
			fetch(`${AppHelper.API_URL}/users/getAll`, options).then(AppHelper.toJSON).then(data => {
				setUser({ 	id: data._id, 
							isAdmin: data.isAdmin,
							firstName: data.firstName,
							lastName: data.lastName,
							email: data.email,
							transactions: data.transactions,
							incomeCategories: data.incomeCategories,
							expenseCategories: data.expenseCategories,
							userBalance: data.userBalance
						})			
			})
	}
	useEffect(() => {
		retrieveUserDetails(accessToken)
	},)
	
	// const { user, setUser } = useContext(UserContext)
	// const transactionsData = user.transactions	
	transactions = transactionsData.map(transactionData => {
	
		return (
			<TransactionCard key={transactionData._id} courseProp={transactionData}/>
		)
	})

	const SearchandFilter = () => {
		const [ searchFormData, setSearchFormData ] = useState({
		searchAmt: "",
		searchType: "",
		searchCategory: "",
		searchNote: "",
		})

		const { transAmt, searchType, searchCategory, searchNote } = searchFormData

		const onSearchChangeHandler = e => {
			setSearchFormData({
				...searchFormData,
				[e.target.name]: e.target.value
				// ,
				// userId: localStorage.getItem('userId')
			})
		}

		return(
			<>															
			<Card>				
				<Card.Body>					
					<h5 className="text-center">Search/Filter</h5>
				</Card.Body>				
			</Card>
			
					{/*<h5 className="text-center">ADD CATEGORY</h5>*/}
					<Form /*onSubmit={ addUserCategory }*/>

						<div className="form-group">
							<input required id="searchNote" name="searchNote" className="form-control"  placeholder="Transaction Purpose" autoComplete="off" onChange={e => onSearchChangeHandler(e)}/>
						</div>			

						<div className="form-group">
                            <select required className="form-control" name="searchType" defaultValue="" onChange={e => onSearchChangeHandler(e)}>
                             <option value="">Select Type</option>
                             	<option value="all">All</option>
                                <option value="expense">Expense</option>
                                <option value="income">Income</option>
                            </select>
                        </div>
						<div className="form-group">
							<Select label="Choose Category" name="searchCategory" options={user.expenseCategories} value={ (user.categories === searchCategory) ? searchCategory : null } onChange={e => onSearchChangeHandler(e)} /> 

							</div>							
					
						<Button className="bg-warning rounded-lg border-white text-dark mb-5" type="submit" block>Search</Button>

					</Form>					
										
			</>
		)//END return() SearchandFilter
	}

	const transactionRows = transactionsData.map(transactionData => {
		return(
			<>		
			<tr key={transactionData._id}>					
				<td>{transactionData.transType}</td>
				<td>{transactionData.transCategory}</td>
				<td>{transactionData.transAmt}</td>
				<td>{transactionData.transDate}</td>
				<td>{transactionData.transNote}</td>
				<td>
					<Button variant="warning">Edit</Button>
					<Button variant="danger">Delete</Button>						
				</td>				
			</tr>
			</>				
		);
	})

	return( /*export default return*/
		<>
		<SearchandFilter />
		{transactionRows}
		</>
	)
}	