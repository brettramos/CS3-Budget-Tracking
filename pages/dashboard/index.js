import { useState, useContext, useEffect } from 'react';
import { Form, Button, Row, Col, Card, Table, Container, Image } from 'react-bootstrap';

import Router from 'next/router';
import Head from 'next/head';
import View from '../../components/View';
import Swal from 'sweetalert2';

import UserContext from '../../UserContext';

import Select from '../../components/Select';

import AppHelper from '../../app-helper';

import AddNewTransaction from '../addNewTransaction/index.js'
import TransactionList from '../dashboard/transactionList/index.js'

const Dashboard = () => {//

	const { user, setUser } = useContext(UserContext)
		const accessToken = localStorage.getItem('token')
	const retrieveUserDetails = (accessToken) => {
			
			const options = {
				headers: { Authorization: `Bearer ${accessToken}` }			
			}
			fetch(`${AppHelper.API_URL}/users/details`, options).then(AppHelper.toJSON).then(data => {
				setUser({ 	id: data._id, 
							isAdmin: data.isAdmin,
							firstName: data.firstName,
							lastName: data.lastName,
							email: data.email,
							transactions: data.transactions,
							incomeCategories: data.incomeCategories,
							expenseCategories: data.expenseCategories,
							userBalance: data.userBalance
						})			
			})
		}
	useEffect(() => {
		retrieveUserDetails(accessToken)
	},)
	return(

		<View title={'Dashboard'}>
			<Row>
				<Col xs={12} md={4}>	
					  	<Profile />
						<AddNewTransaction />	
				</Col>
				<Col xs={12} md={8}>
					<Quote />		
					
				</Col>
			</Row>					  			
		</View>				
	)//END return() Dashboard
}//END const DASHBOARD	

const Profile = () => {

		const { user, setUser } = useContext(UserContext)
		
		return(
			<>															
			<Card>				
				<Card.Body>					
					<h5 className="text-center"></h5>
					<h5 className="text-center">{user.firstName} {user.lastName}</h5>
					<p className="text-center">{user.email}</p>
					<p className="text-center"><strong>Balance: {user.userBalance}</strong></p>
				</Card.Body>				
			</Card>										
			</>
		)//END return() Profile
	} //END const PROFILE	

	const Quote = () => {
		return(
			<>
					<h6 className="text-center pb-5">Try to save something while your salary is small; it’s impossible to save after you begin to earn more. --Jack Benny</h6>
					<Row className="justify-content-center">
					<Col md="4">
		          	<Image src="50smaller-ant-icon-10.jpg" fluid/>
		         	</Col>
				</Row>								
			</>
		)//END return() Quote
	}//END const QUOTE

	const styles = {
		fontFamily: 'sans-serif',
		textAlign: 'center',
	};

export default Dashboard;