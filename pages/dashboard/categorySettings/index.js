import { useState, useContext, useEffect } from 'react';
import { Form, Button, Row, Col, Card } from 'react-bootstrap';
import UserContext from '../../../UserContext';
import users from '../../../data/users';
import Router from 'next/router';
import Head from 'next/head';
import View from '../../../components/View';
import { GoogleLogin } from 'react-google-login';
import AppHelper from '../../../app-helper';
import Swal from 'sweetalert2';

const AddCategory = () => {
	return(
			<View title={'Add Category'}>
				<Row className="justify-content-center">
					<Col xs md="4"> 
					  {/*<h5 className="text-center">BUDGET TRACKER</h5>*/} 
					   <CategoryForm />
					</Col>
				</Row>
			</View>				
		)
}
export default AddCategory;

	const CategoryForm = () => {
		const { user, setUser } = useContext(UserContext)
		// console.log(user)
		
		// const [ newTransCategory, setNewTransCategory] = useState('');
		// const [ password, setPassword] = useState('');
		const [ tokenId, setTokenId] = useState(null);	

		const [ formData, setFormData ] = useState({
			newTransType: "",
			newTransCategory: ""
			})

		const { newTransType, newTransCategory } = formData

		function addUserCategory(e){
			e.preventDefault()

			console.log( 'newTransType:', newTransType, 'newTransCategory: ', newTransCategory);

			if(newTransType !== '' && newTransCategory !== '' ){			
					fetch('http://localhost:4000/api/users/addUserCategory', {
					// fetch('http://localhost:4000/api/expenseCategories', {
						method: 'POST',
						headers: {
							'Content-Type': 'application/json'
						},
						body: JSON.stringify(formData)
					})
					.then(res => {
						return res.json()
					})
					.then(data => {
						console.log(data)
						//lets create a control structure to give a proper response to the user
						if(data === true){
							Swal.fire({
								icon: 'success',
								title: 'New Category Added'
								// ,text: 'You May Now Log In'
							})
						} else {
							Swal.fire({
								icon: 'warning',
								title: 'Something Went Wrong, Check Credentials!'
							})
						}
					})
			
			}else {
				Swal.fire('Something Went Wrong!')
			}		

			e.target.reset()
			const token = localStorage.getItem('token')
			retrieveUserDetails(token)
			console.log(localStorage.getItem('expenseCategories'))
		}//END addUserCategory

		const retrieveUserDetails = (accessToken) => {
			
			const options = {
				headers: { Authorization: `Bearer ${accessToken}` }			
			}
			fetch(`${AppHelper.API_URL}/users/details`, options).then(AppHelper.toJSON).then(data => {
				setUser({ 	id: data._id, 
							isAdmin: data.isAdmin,
							firstName: data.firstName,
							lastName: data.lastName,
							email: data.email,
							transactions: data.transactions,
							incomeCategories: data.incomeCategories,
							expenseCategories: data.expenseCategories						})
				
				localStorage.setItem('expenseCategories', data.expenseCategories)

				console.log(localStorage.getItem('expenseCategories'))

				Router.push('/dashboard')
			})
		}		
		
		const onChangeHandler = e => {
			setFormData({
				...formData,
				[e.target.name]: e.target.value,
				userId: localStorage.getItem('userId')
			})
		}

		return(
				<Card>				
				<Card.Body>
					<h5 className="text-center">ADD CATEGORY</h5>
					<Form onSubmit={ addUserCategory }>

						<div className="form-group">
                            <select required className="form-control" name="newTransType" defaultValue="" onChange={e => onChangeHandler(e)}>
                             <option value="">Expense or Income?</option>
                                <option value="expense">Expense</option>
                                <option value="income">Income</option>
                            </select>
                        </div>
						<div className="form-group">
							<input required id="newTransCategory" name="newTransCategory" className="form-control"  placeholder="New Category" autoComplete="off" onChange={e => onChangeHandler(e)}/>
						</div>							
					
						<Button className="bg-warning rounded-lg border-white text-dark mb-5" type="submit" block> Add Category </Button>

					</Form>
					
				</Card.Body>				
				</Card>
			)
	}		