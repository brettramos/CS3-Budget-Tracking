import { useState } from 'react'
// import toNum from '../../helper/toNum'; 
import { Form, Button, Alert } from 'react-bootstrap'
import DoughnutChart from '../../components/DoughnutChart'; 
import Head from 'next/head'; 

export default function Home({data}) {
  //display the gathered data from rapidAPI in the console. 
  console.log(data) //this will serve as the checker just in case it fails. 
  //lets get the status of the countries from the data variable
  const countriesStats = data.countries_stat
  const [targetCountry, setTargetCountry] = useState(""); 
  const [name, setName] = useState(""); 
const [criticals, setCritical] = useState(0) //critical cases per country
const [deaths, setDeaths] = useState(0) //deaths per country
const [recoveries, setRecoveries ] = useState(0)  //recovered from Covid-19

function search(e) {
  e.preventDefault()
  const match = countriesStats.find(country => country.country_name === targetCountry)
  console.log(match)//lets include a checker
  setName(match.country_name)
  setCritical(toNum(match.serious_critical))
  setDeaths(toNum(match.deaths))
  setRecoveries(toNum(match.total_recovered))
}
	return(
		<div>
      <Head>
         <title>Covid-19 Country Search</title>
      </Head>
			<Form onSubmit={e => search(e)}>
				<Form.Group controlId="country">
					<Form.Label>Country</Form.Label>
					<Form.Control 
						type="text" 
						placeholder="Search for country" 
            value={targetCountry}
            onChange={e => setTargetCountry(e.target.value)}
					/>
					<Form.Text className="text-muted">
						Get Covid-19 stats of searched for country.
					</Form.Text>
				</Form.Group>

				<Button variant="primary" type="submit">
				Submit
				</Button>
			</Form>		
       
        {name !== ''
      ? <>
        <h1>Country: {name}</h1>
        <DoughnutChart criticals={criticals} deaths={deaths} recoveries={recoveries}/>
      </>
      : <Alert variant="info" className="mt-4">Search for a country to visualize its data.</Alert>}
      
    </div>
    )
}

export async function getStaticProps() {
  //fetch data from the covid monitoring endpoint from RapidAPI
  const res = await fetch("https://coronavirus-monitor.p.rapidapi.com/coronavirus/cases_by_country.php", {
    "method": "GET", 
    "headers": {
      "x-rapidapi-host": "coronavirus-monitor.p.rapidapi.com", 
      "x-rapidapi-key": "6085b628a5msh12b4765569d1427p1188bbjsnd3c4dc348539"
    }
  })

  const data = await res.json()

  //lets now return the props
  return {
    props: {
      data
    }
  }
}

