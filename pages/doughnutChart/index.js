import Head from 'next/head'
import Jumbotron from 'react-bootstrap/Jumbotron' 
import DoughnutChart from '../../components/DoughnutChart'; 

export default function Home({data}) {
  console.log(data)
  const incomeArray = data.filter(a => a.transType === 'income');

  const incomeSum = incomeArray.reduce((sum, current) => sum + current.transAmt, 0);  // console.log(incomeSum)  

  const expenseArray = data.filter(a => a.transType === 'expense');
  const expenseSum = expenseArray.reduce((sum, current) => sum + current.transAmt, 0);

  const expenseMap = expenseArray.map(x => x.transAmt)
  console.log(expenseMap)
  const incomeMap = incomeArray.map(x => x.transAmt)
  console.log(incomeMap)

  return (
    <div>
      <Head>
        <title>Covid-19 Tracker App</title>
        <link rel="icon" href="/favicon.ico" />
      </Head> 
        <DoughnutChart income={incomeSum} expense={expenseSum}/>
    
    </div>
  )
}

export async function getStaticProps() {
  const res = await fetch('http://localhost:4000/api/transactions')

  const data = await res.json()
  console.log(data)

  return {
    props: {
      data
    }
  }
}