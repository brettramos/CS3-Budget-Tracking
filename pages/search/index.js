import { useContext, useState, useEffect } from 'react';
import TransactionRow from '../../components/TransactionRow';
import { Table, Button, Container, Form } from 'react-bootstrap';

import UserContext from '../../UserContext';
import Head from 'next/head';

import Select from '../../components/Select';

export async function getServerSideProps(){
   const res = await fetch('http://localhost:4000/api/transactions')
                    
   const transactionsData = await res.json()

   return { props: { transactionsData }}
}

export default function index({transactionsData}){

  console.log(transactionsData)

  const [ formData, setFormData ] = useState({   
    searchItem: ""
    })

  const { searchItem } = formData

    const onChangeHandler = e => {
      setFormData({
        ...formData,
        [e.target.name]: e.target.value

      })
    }

    let match = []
  function search(e) {
    e.preventDefault()
    match = transactionsData.filter(transaction => transaction.transAmt == searchItem)
    console.log(match)

  }

  const transactions = match.map(transactionData => {
    return (
        <TransactionRow key={match._id} transactionProp={match}/>
      )
  }) 
    
  const transactionRows = match.map(match => {

    return(
        <tr key={match._id}>
            <td>{match._id}</td>
            <td>{match.transType}</td>
            <td>{match.transCategory}</td>
            <td>{match.transAmt}</td>
            <td>{match.transDate}</td>
            <td>{match.transNote}</td>
        </tr>
      )
  })
   
    return (
          //  
            <Container>
             <Head>
                 <title>Search</title>
             </Head>

            <>
            <Form onSubmit={e => search(e)}>        
              
              <div className="form-group">
                <input required type="text" id="searchItem" name="searchItem" className="form-control mt-5" placeholder="What are you looking for?" autoComplete="off" value={searchItem} onChange={e => onChangeHandler(e)}/>
              </div>

              <Button type="submit" className="btn btn-success mb-5" block>Search</Button>             
            </Form>
            </>

              <Table striped bordered hover>
                    <thead>
                       <tr>
                            <th>Type</th>
                            <th>Category</th>
                            <th>Amount</th>
                            <th>Date</th>
                            <th>Purpose</th>                   
                       </tr>
                    </thead>          
                <tbody>
             { transactions }             
                </tbody>
              </Table>
            </Container>    
    )
}
