import { Row, Col, Card } from 'react-bootstrap';//lets acquire the bootstrap grid system
//lets also acquire the Card component from bootstrap
// import Card from 'react-bootstrap/Card';

//lets create a Highlights() to build our Highights components
export default function Highlights () {
	return(
		<Row>
			<Col>
				<blockquote className="blockquote mb-0 card-body">
			      <p className="text-center">
			        The only thing I shall want for a rainy day will be my umbrella.
			      </p>
			      <footer className="blockquote-footer text-center">
			        <small className="text-muted">
			          Agatha Christie in  <cite title="Nemesis">Nemesis</cite>
			        </small>
			      </footer>
			    </blockquote>
				<h3 className="text-center"></h3>
			</Col>			
		</Row>
	)
}