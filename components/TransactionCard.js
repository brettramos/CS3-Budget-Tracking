import { useState, useEffect } from 'react';
import { Card, Button } from 'react-bootstrap';
import PropTypes from 'prop-types';
import AppHelper from '../app-helper';//lets acquire first the apphelper module
import Swal from 'sweetalert2';

//lets create a state hook for our course component: useState()
export default function TransactionCard({ transProp }){
	const { transType, transCategory, transAmt, transDate, transNote, transId, transAddedOn } = transProp;

	// const [count, setCount] = useState(0);
	// const [seats, setSeats] = useState(20);
	// const [isOpen, setIsOpen] = useState(true);
	//after declaring the first state hook lets create another on so that in our simulation, everytime the number of enrollees increase the number of seats available decrease

	function enroll(transId){//this function will create a simulation of an enrollment page
		console.log(transId)//catch the data to make sure theres a value
		// fetch(`${ AppHelper.API_URL }/users/enroll`, {
		// 	method: 'POST',
		// 	headers: {
		// 		'Content-Type': 'application/json',
		// 		'Authorization': `Bearer ${localStorage.getItem('token')}`
		// 	},
		// 	body: JSON.stringify({
		// 		courseId: courseId,
		// 		googleToken: localStorage.getItem('googleToken')
		// 	})
		// })
		// .then(res => {
		// 	console.log(res)
		// 	return res.json()
		// })
		// .then(data => {
		// 	console.log(data)
		// 	Swal.fire({
		// 		icon: 'success',
		// 		title: 'Enrolled Successfully!',
		// 		text: 'You will be receiving an email message for verification'
		// 	})
		// })
	}

	//our next task is to create a "side effect" that will disable the enroll button once the number of seats has reached our limit.
	// useEffect(() => {
	// 	//lets create a control structure inside that will determine if the seat count has reached its limit.
	// 	if(seats === 0){
	// 		//identify the proper return if the condition is met.
	// 		setIsOpen(false);
	// 	}
	// }, [seats])
	return(
		<Card>
			<Card.Body>
				<Card.Title>{transNote}</Card.Title>
				<Card.Text>					
					<span className="subtitle">Amount: </span>
					PHP {transProp.transAmt}
					<br />
					<span className="subtitle">Type: </span>
					{transType}
					<br />
					<span className="subtitle">Category: </span>
					{transCategory}
					<br />
					<span className="subtitle">Transaction Date: </span>
					{transDate}
					<br />					
				</Card.Text>
				{/*}
				{isOpen ?
					<Button className="bg-primary" onClick={() => {enroll(courseProp._id)}}>Enroll</Button>
					:
					<Button className="bg-primary" disabled>Not Available</Button>	
				}
				*/}							
			</Card.Body>
		</Card>
		)
}

TransactionCard.propTypes = {

	transaction: PropTypes.shape({
		transType: PropTypes.string.isRequired,
		transCategory: PropTypes.string.isRequired,
		transAmt: PropTypes.string.isRequired,
		transDate: PropTypes.string.isRequired,
		transNote: PropTypes.string.isRequired,
		transId: PropTypes.string.isRequired,
		transAddedOn: PropTypes.string.isRequired
	})	
}