import  { useState, useEffect } from 'react'; 
import { Table, Button, Container } from 'react-bootstrap';
import PropTypes from 'prop-types'; 
import AppHelper from '../app-helper';
import Swal from 'sweetalert2' 

export default function TransactionRow({ transactionProp }){ 
  const { transType, transCategory, transAmt, transDate, transNote } = transactionProp; 	 

	return(
		
				    <tr>
				    <td>{transType}</td>
				      <td>{transCategory}</td>
				      <td>PHP {transAmt}</td>
				      <td>{transDate}</td>
				      <td>{transNote}</td>
				    </tr>				   
			      
		)
}

TransactionRow.propTypes = {
	transactionProp: PropTypes.shape({
		transType: PropTypes.string.isRequired,
		transCategory: PropTypes.string.isRequired,
		transAmt: PropTypes.number.isRequired
	})
}
