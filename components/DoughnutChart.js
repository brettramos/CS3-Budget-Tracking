import { Doughnut } from "react-chartjs-2";

export default function DoughnutChart({income, expense}) {
    return (
    	<Doughnut data={{
    		datasets: [{
    			data: [income, expense],
    			backgroundColor: ["green", "red"]
    		}],
    		labels: [
    			'Income', 
    			'Expenses'
    		]
    	}} redraw={ false }/>  	
   )
}

