import { Form, Button, Row, Col, Card, select } from 'react-bootstrap';
const Select = ({ name , value, options, onChange }) => {

  return (
    <div>
      <div>
        <div>
          <select className="form-control" name={name} value={value} onChange={onChange}>
              {options.map((option) => (
                <option key={option.cat_name} value={option.cat_name}>
                  {option.cat_name}
                </option>
              ))}
          </select>
        </div>
      </div>
    </div>
  )
}

export default Select