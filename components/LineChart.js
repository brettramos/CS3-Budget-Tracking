import { Line } from "react-chartjs-2";

export default function LineChart({income, expense}) {
    return (
    	<Line data={{
    		datasets: [{
    			data: [income, expense],
    			backgroundColor: ["green", "red"],
                borderColor: ["green", "red"],
                fill: false,
                  lineTension: 0.1,
                  backgroundColor: ["green", "red"],
                  borderColor: ["green", "red"],
                  borderCapStyle: 'butt',
                  borderDash: [],
                  borderDashOffset: 0.0,
                  borderJoinStyle: 'miter',
                  pointBorderColor: ["green", "red"],
                  pointBackgroundColor: '#fff',
                  pointBorderWidth: 1,
                  pointHoverRadius: 5,
                  pointHoverBackgroundColor: ["green", "red"],
                  pointHoverBorderColor: ["green", "red"],
                  pointHoverBorderWidth: 2,
                  pointRadius: 1,
                  pointHitRadius: 10
    		}],
    		labels: [
    			'Income', 
    			'Expense'
    		]
    	}} redraw={ false }/>  	
   )
}
