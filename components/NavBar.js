import { useContext } from 'react';
import Navbar from 'react-bootstrap/Navbar';
import Nav from 'react-bootstrap/Nav';
import Link from 'next/link'; 
import UserContext from '../UserContext';

export default function NavBar(){
	
	const { user } = useContext(UserContext);
	
	return (
			<Navbar bg="white" expand="lg">
			<Link href="/">
				<a className="navbar-brand">
				ANT
				</a>
			</Link>
			<Navbar.Toggle aria-controls="basic-navbar-nav"/>
			<Navbar.Collapse id="basic-navbar-nav">		

				{(user.email !== null)
					?					 
					 <>
					 	<Link href="/dashboard">
							<a className="nav-link">Home</a>
						</Link>						

						<Link href="/search">
							<a className="nav-link" role="button">Search</a>
						</Link>
						
						<Link href="/doughnutChart">
							<a className="nav-link" role="button">Analytics</a>
						</Link>

						<Link href="/transactions">
							<a className="nav-link" role="button">Transactions</a>
						</Link>

						<Link href="/logout">
							<a className="nav-link" role="button">Logout</a>
						</Link>
					 </>
					:
					<>
					<Link href="/register">
						<a className="nav-link" role="button">Register</a>
					</Link>
					<Link href="/login">
						<a className="nav-link" role="button">Login</a>
					</Link>
					</>
				}				
						
			</Navbar.Collapse>					
			</Navbar>
		)
}