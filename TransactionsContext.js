import React from 'react';//we are going to create a context data for our app

//1. lets create a Context Object and we will place it inside a variable called UserContext
const TransactionsContext = React.createContext();

//2. lets acquire its Provider component which will allow us to consume components scubscribed to context changes
export const TransactionsProvider = TransactionsContext.Provider;

export default TransactionsContext;